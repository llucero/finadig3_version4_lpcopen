#include "ciaaUART.h"
#include "board.h"
#include "FreeRTOS.h"
#include "task.h"
#include "chip.h"
#include "arm_math.h"
#include "arm_const_structs.h"
#include "arm_common_tables.h"
#include "queue.h"
#include "cmsis.h"
#include "semphr.h"



#define PORT(n)		((uint8_t) n)
#define PIN(n)		((uint8_t) n)
#define GRUPO(n)	((uint8_t) n)
#define ADC_0		0
#define CHANNEL_1	0
#define UARTx_IRQn  UART3_IRQn
#define UARTx_IRQHandler UART3_IRQHandler
#define _GPDMA_CONN_UART_Tx GPDMA_CONN_UART3_Tx
#define _GPDMA_CONN_UART_Rx GPDMA_CONN_UART3_Rx
#define numSamples 2048
#define numOutFFT numSamples/2
#define numIteraciones 64
#define lowBand125Hz 2
#define highBand125Hz lowBand250Hz
#define lowBand250Hz highBand250Hz/2
#define highBand250Hz lowBand500Hz
#define lowBand500Hz highBand500Hz/2
#define highBand500Hz lowBand1kHz
#define lowBand1kHz highBand1kHz/2
#define highBand1kHz lowBand2kHz
#define lowBand2kHz highBand2kHz/2
#define highBand2kHz lowBand4kHz
#define lowBand4kHz highBand4kHz/2
#define highBand4kHz lowBand8kHz
#define lowBand8kHz highBand8kHz/2
#define highBand8kHz lowBand16kHz
#define lowBand16kHz highBand16kHz/2
#define highBand16kHz numOutFFT
#define Hz125 0
#define Hz250 1
#define Hz500 2
#define Hz1000 3
#define Hz2000 4
#define Hz4000 5
#define Hz8000 6
#define Hz16000 7
#define slotAvg 0
#define slotMax 1



//Definio cola y semaforos
QueueHandle_t ProcesaDatos_Hdl;
QueueHandle_t SalidaDatos_Hdl;
SemaphoreHandle_t dataReadySemaphore;
SemaphoreHandle_t packetReadySemaphore;

float FFTInput [numSamples];
int indexSample = 0;
float salidaMax[8];
float salidaAvg[8];

static uint8_t dmaChannelNumTx, dmaChannelNumRx;
static uint32_t dma_buffer[numSamples];
static uint16_t adc_buffer[numSamples];
static uint8_t dma_ch_adc;  /* Hay 8 canales DMA disponibles*/
static uint8_t dma_tc_adc;  // TC: Terminal de conteo
static volatile uint8_t Burst_Mode_Flag = 0;


//Definiciones UART
uint8_t rxbuf[3][UART_BUF_SIZE];
uint8_t txbuf[3][UART_BUF_SIZE];

RINGBUFF_T rrb[10]; /* Transmit and receive ring buffers */
RINGBUFF_T trb[10];

uartData_t uarts[4] =
{
		{ LPC_USART0, &(rrb[0]), &(trb[0]) },
		{ LPC_USART2, &(rrb[1]), &(trb[1]) },
		{ LPC_USART3, &(rrb[2]), &(trb[2]) },
};


//Configuramos ADC y UART
static void prvSetupHardware(void)
{
	SystemCoreClockUpdate();
	Chip_GPIO_Init(LPC_GPIO_PORT);


		Chip_SCU_PinMux( GRUPO(1) , PIN(0) , SCU_MODE_INACT | SCU_MODE_INBUFF_EN | SCU_MODE_ZIF_DIS , SCU_MODE_FUNC0 );
		Chip_SCU_PinMux( GRUPO(1) , PIN(1) , SCU_MODE_INACT | SCU_MODE_INBUFF_EN | SCU_MODE_ZIF_DIS , SCU_MODE_FUNC0 );
		Chip_SCU_PinMux( GRUPO(1) , PIN(2) , SCU_MODE_INACT | SCU_MODE_INBUFF_EN | SCU_MODE_ZIF_DIS , SCU_MODE_FUNC0 );
		Chip_SCU_PinMux( GRUPO(1) , PIN(6) , SCU_MODE_INACT | SCU_MODE_INBUFF_EN | SCU_MODE_ZIF_DIS , SCU_MODE_FUNC0 );
		Chip_SCU_PinMux( GRUPO(2) , PIN(12) , SCU_MODE_INACT | SCU_MODE_INBUFF_EN | SCU_MODE_ZIF_DIS , SCU_MODE_FUNC0 );

		Chip_SCU_PinMux(GRUPO(2), PIN(0), SCU_MODE_INACT | SCU_MODE_INBUFF_EN | SCU_MODE_ZIF_DIS, SCU_MODE_FUNC4);//LED_R
		Chip_SCU_PinMux(GRUPO(2), PIN(1), SCU_MODE_INACT | SCU_MODE_INBUFF_EN | SCU_MODE_ZIF_DIS, SCU_MODE_FUNC4);//LED_G
		Chip_SCU_PinMux(GRUPO(2), PIN(2), SCU_MODE_INACT | SCU_MODE_INBUFF_EN | SCU_MODE_ZIF_DIS, SCU_MODE_FUNC4); //LED_B

		Chip_GPIO_SetPinDIRInput(LPC_GPIO_PORT , PORT(0) , PIN(4));
		Chip_GPIO_SetPinDIRInput(LPC_GPIO_PORT , PORT(0) , PIN(8));
		Chip_GPIO_SetPinDIRInput(LPC_GPIO_PORT , PORT(0) , PIN(9));

		Chip_GPIO_SetPinDIROutput(LPC_GPIO_PORT , PORT(1) , PIN(11));
		Chip_GPIO_SetPinDIROutput(LPC_GPIO_PORT , PORT(1) , PIN(12));
		Chip_GPIO_SetPinDIROutput(LPC_GPIO_PORT , PORT(0) , PIN(14));

		Chip_GPIO_SetPinState(LPC_GPIO_PORT, PORT(1) , PIN(11) , (bool) false);//led2 Inicializo en 0
		Chip_GPIO_SetPinState(LPC_GPIO_PORT, PORT(1) , PIN(12) , (bool) false);//led3 Inicializo en 0
		Chip_GPIO_SetPinState(LPC_GPIO_PORT, PORT(0) , PIN(14) , (bool) false); //led1 Inicializo en 0
		Chip_GPIO_SetPinState(LPC_GPIO_PORT, PORT(5), PIN(0), (bool) false); //ledR Inicializo en 0
		Chip_GPIO_SetPinState(LPC_GPIO_PORT, PORT(5), PIN(1), (bool) false); //ledG Inicializo en 0
		Chip_GPIO_SetPinState(LPC_GPIO_PORT, PORT(5), PIN(2), (bool) false); //ledB Inicializo en 0



    //Configuracion del ADC0 -  Canal 1

	//Definiciones ADC

		uint32_t 					ADC_bitRate =  48000;
		ADC_RESOLUTION_T 			ADC_bitAccuracy = ADC_10BITS;
		ADC_CLOCK_SETUP_T ADC_setup;

    Chip_ADC_Init(LPC_ADC0, &ADC_setup);								//Inicializamos el ADC1
    Chip_ADC_EnableChannel(LPC_ADC0, ADC_CH1, ENABLE);				//Habilitamos el canal 1
   	Chip_ADC_SetSampleRate(LPC_ADC0, &ADC_setup, ADC_bitRate);		//Configuramos tasa de muestreo
   	Chip_ADC_SetResolution(LPC_ADC0, &ADC_setup, ADC_bitAccuracy);	//Configuramos resolución
   	Chip_ADC_SetBurstCmd(LPC_ADC0, ENABLE);						//Conversión continua inhabilitada
   	Chip_ADC_Int_SetChannelCmd(LPC_ADC0, ADC_CH1, ENABLE);		//Habilitamos interrupción para el canal 1
   	Chip_ADC_SetStartMode(LPC_ADC0, ADC_START_NOW, ADC_TRIGGERMODE_RISING); 	//Inicio conversion del ADC


	/* UART3 (RS232) */
	Chip_UART_Init(LPC_USART3);
	Chip_UART_SetBaud(LPC_USART3, 9600);

	Chip_UART_TXEnable(LPC_USART3);

	Chip_SCU_PinMux(2, 3, MD_PDN, FUNC2);              /* P2_3: UART3_TXD */
	Chip_SCU_PinMux(2, 4, MD_PLN|MD_EZI|MD_ZI, FUNC2); /* P2_4: UART3_RXD */

	Chip_UART_IntEnable(LPC_USART3, UART_IER_RBRINT);
	NVIC_EnableIRQ(USART3_IRQn);

	RingBuffer_Init(uarts[2].rrb, rxbuf[2], 1, UART_BUF_SIZE);
	RingBuffer_Init(uarts[2].trb, txbuf[2], 1, UART_BUF_SIZE);
}

/* Inicializo DMA para UART, habilito DMA controlo y habilito DMA por interrupcion */

void DMA_Init(void){

   Chip_GPDMA_Init(LPC_GPDMA);
   LPC_GPDMA->SYNC = 0x00;

   NVIC_SetPriority(DMA_IRQn, ((0x01 << 3) | 0x01));
   NVIC_EnableIRQ(DMA_IRQn);
   NVIC_ClearPendingIRQ(DMA_IRQn);
   NVIC_EnableIRQ(DMA_IRQn);

   Chip_ADC_Int_SetChannelCmd(LPC_ADC0, ADC_CH1, ENABLE);

   dma_ch_adc = Chip_GPDMA_GetFreeChannel(LPC_GPDMA, GPDMA_CONN_ADC_0);
   if (Burst_Mode_Flag) {
      Chip_ADC_SetBurstCmd(LPC_ADC0, ENABLE);
   }

   if (!Burst_Mode_Flag) {
      Chip_ADC_SetStartMode(LPC_ADC0, ADC_START_NOW, ADC_TRIGGERMODE_RISING);
   }}

static void App_DMA_DeInit(void)
   {
   	Chip_GPDMA_Stop(LPC_GPDMA, dmaChannelNumTx);
   	Chip_GPDMA_Stop(LPC_GPDMA, dmaChannelNumRx);
   	NVIC_DisableIRQ(DMA_IRQn);
   }

void startDMATransfer(void)
{
    dma_tc_adc = 0;

    Chip_GPDMA_Transfer(LPC_GPDMA, dma_ch_adc,
            GPDMA_CONN_ADC_0, (uint32_t)&dma_buffer,
            GPDMA_TRANSFERTYPE_P2M_CONTROLLER_DMA, numSamples);

}

void DMA_IRQHandler(void)
{
    if (Chip_GPDMA_Interrupt(LPC_GPDMA, dma_ch_adc) == SUCCESS) {
        dma_tc_adc = 1;
    }
}

void ADC0_IRQHandler (void)
{
	NVIC_ClearPendingIRQ(ADC0_IRQn); //Limpio la interrupcion del ADC1
	BaseType_t xHigherPriorityTaskWoken = pdFALSE;

	portYIELD_FROM_ISR( xHigherPriorityTaskWoken );

}


/*****************************************************************
 * Tarea que realiza la FFT
 ****************************************************************/
static void vFFTTask (void * pvParameters)
{
	while(1)

	{

			float32_t FFTOutput [numOutFFT];
			uint32_t fftSize = numOutFFT;
			uint32_t ifftFlag = 0;
			uint32_t i = 0;
			uint32_t doBitReverse = 1;
			xSemaphoreTake(dataReadySemaphore, portMAX_DELAY);
			xSemaphoreTake(packetReadySemaphore, portMAX_DELAY);
			uint8_t iteracion=0;
			float banda125Hz[numIteraciones];
			float banda250Hz[numIteraciones];
			float banda500Hz[numIteraciones];
			float banda1kHz[numIteraciones];
			float banda2kHz[numIteraciones];
			float banda4kHz[numIteraciones];
			float banda8kHz[numIteraciones];
			float banda16kHz[numIteraciones];
			uint32_t unused;

			for(;;)
			{
				if (dma_tc_adc) {
				if (xSemaphoreTake(dataReadySemaphore, portMAX_DELAY) == pdTRUE){
				for (i = 0; i < numSamples; i++){ // Elimino la continua de la medicion
						adc_buffer[i]=ADC_DR_RESULT(dma_buffer[i]);
						dma_buffer[i] = FFTInput[i];
						FFTInput[i] = FFTInput[i] -2048.0;
					}

					arm_cfft_f32(&arm_cfft_sR_f32_len1024, FFTInput, ifftFlag, doBitReverse);
					arm_cmplx_mag_f32(FFTInput, FFTOutput, fftSize); // Termino cálculo de FFT
					for (i = 0; i < numOutFFT; i++){
						FFTOutput[i] = 20*((float)log10((double)FFTOutput[i])); // Paso a decibeles
					}
					for (i=lowBand125Hz; i <highBand125Hz; i++){ // Genero las columnas de frecuencias, según lo utilizado en la industria de audio
						if (banda125Hz[iteracion] < FFTOutput[i]) banda125Hz[iteracion] = FFTOutput[i]; // Asigno el máximo de cada bloque a la frecuencia
					}
					for (i=lowBand250Hz; i <highBand250Hz; i++){
						if (banda250Hz[iteracion] < FFTOutput[i]) banda250Hz[iteracion] = FFTOutput[i];
					}
					for (i=lowBand500Hz; i <highBand500Hz; i++){
						if (banda500Hz[iteracion] < FFTOutput[i]) banda500Hz[iteracion] = FFTOutput[i];
					}
					for (i=lowBand1kHz; i <highBand1kHz; i++){
						if (banda1kHz[iteracion] < FFTOutput[i]) banda1kHz[iteracion] = FFTOutput[i];
					}
					for (i=lowBand2kHz; i <highBand2kHz; i++){
						if (banda2kHz[iteracion] < FFTOutput[i]) banda2kHz[iteracion] = FFTOutput[i];
					}
					for (i=lowBand4kHz; i <highBand4kHz; i++){
						if (banda4kHz[iteracion] < FFTOutput[i]) banda4kHz[iteracion] = FFTOutput[i];
					}
					for (i=lowBand8kHz; i <highBand8kHz; i++){
						if (banda8kHz[iteracion] < FFTOutput[i]) banda8kHz[iteracion] = FFTOutput[i];
					}
					for (i=lowBand16kHz; i <highBand16kHz; i++){
						if (banda16kHz[iteracion] < FFTOutput[i]) banda16kHz[iteracion] = FFTOutput[i];
					}
					iteracion = (iteracion + 1) % numIteraciones;
					if (iteracion == 0){
						arm_mean_f32(banda125Hz, numIteraciones, &salidaAvg[Hz125]); // Obtengo promedio de las 64 iteraciones
						arm_max_f32(banda125Hz, numIteraciones, &salidaMax[Hz125], &unused); // Obtengo maximo de las 64 iteraciones
						arm_mean_f32(banda250Hz, numIteraciones, &salidaAvg[Hz250]);
						arm_max_f32(banda250Hz, numIteraciones, &salidaMax[Hz250], &unused);
						arm_mean_f32(banda500Hz, numIteraciones, &salidaAvg[Hz500]);
						arm_max_f32(banda500Hz, numIteraciones, &salidaMax[Hz500], &unused);
						arm_mean_f32(banda1kHz, numIteraciones, &salidaAvg[Hz1000]);
						arm_max_f32(banda1kHz, numIteraciones, &salidaMax[Hz1000], &unused);
						arm_mean_f32(banda2kHz, numIteraciones, &salidaAvg[Hz2000]);
						arm_max_f32(banda2kHz, numIteraciones, &salidaMax[Hz2000], &unused);
						arm_mean_f32(banda4kHz, numIteraciones, &salidaAvg[Hz4000]);
						arm_max_f32(banda4kHz, numIteraciones, &salidaMax[Hz4000], &unused);
						arm_mean_f32(banda8kHz, numIteraciones, &salidaAvg[Hz8000]);
						arm_max_f32(banda8kHz, numIteraciones, &salidaMax[Hz8000], &unused);
						arm_mean_f32(banda16kHz, numIteraciones, &salidaAvg[Hz16000]);
						arm_max_f32(banda16kHz, numIteraciones, &salidaMax[Hz16000], &unused);
						vTaskDelay((TickType_t) 5000);
						xSemaphoreGive(packetReadySemaphore);	// Envio a UART Task
					}
					else {
						vTaskDelay((TickType_t) 8000); // Demora entre paquetes de 2048 muestras

					}
				}
			vTaskDelay ((TickType_t) 100);
	}}}}


/***************************************************************
 * Testeo de UART- borrar cuando llegue su momento.
 *************************************************************/
static void vUARTTask(void *pvParameters)
{
	while(1){
			char newline [] = "\r\n";
			char initHeaderMax[] = "<9";
			char initHeaderAvg[] = "<6";
			char endHeader[] = ">";
			char separator[] = ",";
			char buffer[16];
			for(;;)
			{
				if (xSemaphoreTake( packetReadySemaphore, portMAX_DELAY) == pdTRUE){

			   Chip_GPDMA_Transfer(LPC_GPDMA, dmaChannelNumTx, (uint32_t)&initHeaderMax[0], _GPDMA_CONN_UART_Tx,
					   	   	       GPDMA_TRANSFERTYPE_M2P_CONTROLLER_DMA, sizeof(initHeaderMax));

					for (int aux = 0; aux < 8; aux++){

				Chip_GPDMA_Transfer(LPC_GPDMA, dmaChannelNumTx, (uint32_t)&separator[0], _GPDMA_CONN_UART_Tx,
									GPDMA_TRANSFERTYPE_M2P_CONTROLLER_DMA, sizeof(separator));

				Chip_GPDMA_Transfer(LPC_GPDMA, dmaChannelNumTx, (uint32_t)&buffer[0], _GPDMA_CONN_UART_Tx,
									GPDMA_TRANSFERTYPE_M2P_CONTROLLER_DMA, sizeof(buffer));
							}

				Chip_GPDMA_Transfer(LPC_GPDMA, dmaChannelNumTx, (uint32_t)&endHeader[0], _GPDMA_CONN_UART_Tx,
									GPDMA_TRANSFERTYPE_M2P_CONTROLLER_DMA, sizeof(endHeader));

				Chip_GPDMA_Transfer(LPC_GPDMA, dmaChannelNumTx, (uint32_t)&newline[0], _GPDMA_CONN_UART_Tx,
									GPDMA_TRANSFERTYPE_M2P_CONTROLLER_DMA, sizeof(newline));

				Chip_GPDMA_Transfer(LPC_GPDMA, dmaChannelNumTx, (uint32_t)&initHeaderAvg[0], _GPDMA_CONN_UART_Tx,
									GPDMA_TRANSFERTYPE_M2P_CONTROLLER_DMA, sizeof(initHeaderAvg));

						for (int aux = 0; aux < 8; aux++){

				Chip_GPDMA_Transfer(LPC_GPDMA, dmaChannelNumTx, (uint32_t)&separator[0], _GPDMA_CONN_UART_Tx,
									GPDMA_TRANSFERTYPE_M2P_CONTROLLER_DMA, sizeof(separator));
				Chip_GPDMA_Transfer(LPC_GPDMA, dmaChannelNumTx, (uint32_t)&buffer[0], _GPDMA_CONN_UART_Tx,
									GPDMA_TRANSFERTYPE_M2P_CONTROLLER_DMA, sizeof(buffer));

							}

						Chip_GPDMA_Transfer(LPC_GPDMA, dmaChannelNumTx, (uint32_t)&endHeader[0], _GPDMA_CONN_UART_Tx,
											GPDMA_TRANSFERTYPE_M2P_CONTROLLER_DMA, sizeof(endHeader));

						Chip_GPDMA_Transfer(LPC_GPDMA, dmaChannelNumTx, (uint32_t)&newline[0], _GPDMA_CONN_UART_Tx,
											GPDMA_TRANSFERTYPE_M2P_CONTROLLER_DMA, sizeof(newline));

						vTaskDelay((TickType_t) 60000);

					}
				vTaskDelay((TickType_t) 1000);
		  	}
	}
}





int main(void)
{
	prvSetupHardware();
	App_DMA_DeInit();
	ADC0_IRQHandler ();
	DMA_Init();
	startDMATransfer();

	vSemaphoreCreateBinary(dataReadySemaphore );
	vSemaphoreCreateBinary(packetReadySemaphore);
	xSemaphoreTake ( dataReadySemaphore , portMAX_DELAY);
	ProcesaDatos_Hdl = xQueueCreate( 1, sizeof (float) );
	SalidaDatos_Hdl= xQueueCreate( 1, sizeof (float) );


	/* UART output thread, simply counts seconds */
	xTaskCreate(vUARTTask, "vTaskUart", configMINIMAL_STACK_SIZE,
			NULL, (tskIDLE_PRIORITY + 1UL), (TaskHandle_t *) NULL);

	xTaskCreate(vFFTTask, "vTaskFFT", configMINIMAL_STACK_SIZE,
			NULL, (tskIDLE_PRIORITY + 1UL), (TaskHandle_t *) NULL);



	/* Start the scheduler */
	vTaskStartScheduler();


	return 1;
}
